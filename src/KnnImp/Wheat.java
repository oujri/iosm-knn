package KnnImp;

// model instances (features + class)
public class Wheat {

	double[] wheatAttributes;
	String wType;
	
	public Wheat(double[] wheatAttributes, String wType){
		this.wheatAttributes = wheatAttributes;
		this.wType = wType;	    	    
	}
	
}
