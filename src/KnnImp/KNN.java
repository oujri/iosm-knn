package KnnImp;

import java.util.*;

class KNN
{
	// input data
	private double area;
	private double peremeter;
	private double compactness;
	private double lengthK;
	private double width;
	private double asymmetry;
	private double lengthKG;
	private int k;
	
	private double[][] instances; 	
	private String[] check;
		
	public KNN (double[][] instances, String[] check, int k, double area, double peremeter, double compactness, double lengthK, double width, double asymmetry, double lengthKG) {
		this.instances = instances;
		this.check = check;
		this.k = k;
		
		this.area = area;
		this.peremeter = peremeter;
		this.asymmetry = asymmetry;
		this.compactness = compactness;
		this.lengthK = lengthK;
		this.width = width;
		this.asymmetry = asymmetry;
		this.lengthKG = lengthKG;
	}

	private static String findMajorityClass(String[] array)
	{
		System.out.println("\nFinding Wheat class");
		//add the String array to a HashSet to get unique String values
		Set<String> h = new HashSet<String>(Arrays.asList(array));
		//convert the HashSet back to array
		String[] uniqueValues = h.toArray(new String[0]);
		//counts for unique strings
		int[] counts = new int[uniqueValues.length];
		// loop thru unique strings and count how many times they appear in original array   
		for (int i = 0; i < uniqueValues.length; i++) {
			for (int j = 0; j < array.length; j++) {
				if(array[j].equals(uniqueValues[i])){
					counts[i]++;
				}
			}        
		}

		/*for (int i = 0; i < uniqueValues.length; i++)
			System.out.println(uniqueValues[i]);
		for (int i = 0; i < counts.length; i++)
			System.out.println(counts[i]);*/


		int max = counts[0];
		for (int counter = 1; counter < counts.length; counter++) {
			if (counts[counter] > max) {
				max = counts[counter];
			}
		}
		
		System.out.println("max # of occurences: " + max);

		// how many times max appears
		// we know that max will appear at least once in counts
		// so the value of freq will be 1 at minimum after this loop
		int freq = 0;
		for (int counter = 0; counter < counts.length; counter++) {
			if (counts[counter] == max) {
				freq++;
			}
		}

		// index of most freq value if we have only one mode
		int index = -1;
		if(freq==1){
			for (int counter = 0; counter < counts.length; counter++) {
				if (counts[counter] == max) {
					index = counter;
					break;
				}
			}
			//System.out.println("one majority class, index is: "+index);
			return uniqueValues[index];
		} else{//we have multiple modes
			int[] ix = new int[freq];//array of indices of modes
			System.out.println("multiple majority classes: "+freq+" classes");
			int ixi = 0;
			for (int counter = 0; counter < counts.length; counter++) {
				if (counts[counter] == max) {
					ix[ixi] = counter;//save index of each max count value
					ixi++; // increase index of ix array
				}
			}

			for (int counter = 0; counter < ix.length; counter++)         
				System.out.println("class index: "+ix[counter]);       

			//now choose one at random
			Random generator = new Random();        
			//get random number 0 <= rIndex < size of ix
			int rIndex = generator.nextInt(ix.length);
			System.out.println("random index: "+rIndex);
			int nIndex = ix[rIndex];
			//return unique value at that index 
			return uniqueValues[nIndex];
		}

	}

	public void main(){ 
		
		System.out.println("Start KNN calculations : ");
		
		//list to save Wheat data
		List<Wheat> list = new ArrayList<Wheat>();
		//list to save distance result
		List<Result> resultList = new ArrayList<Result>();
		// add Wheat data to WheatList       
		for(int i = 0; i < instances.length; i++) {
			list.add(new Wheat(instances[i],check[i]));
		}
		
		//data about unknown Wheat
		double[] query = {area, peremeter, compactness, lengthK, width, asymmetry, lengthKG, 1};
		//find distances
		for(Wheat w : list){
			double dist = 0.0;  
			for(int j = 0; j < w.wheatAttributes.length; j++){
				dist += Math.pow(w.wheatAttributes[j] - query[j], 2) ;
				//System.out.print(city.cityAttributes[j]+" ");    	     
			}
			double distance = Math.sqrt( dist );
			resultList.add(new Result(distance, w.wType));
			//System.out.println(distance);
		} 

		Collections.sort(resultList, new DistanceComparator());
		String[] ss = new String[k];
		System.out.println("Nearest neighbors Wheats");
		for(int x = 0; x < k; x++){
			System.out.println("Wheat: class = " + resultList.get(x).typeW +", distance = " + (resultList.get(x).distance - 1));
			//get classes of k nearest instances (wheat type) from the list into an array
			ss[x] = resultList.get(x).typeW;
		}
		String majClass = findMajorityClass(ss);
		System.out.println("Class of new instance is: "+majClass); 
	}

	
	//compare results via distances
	static class DistanceComparator implements Comparator<Result> {
		@Override
		public int compare(Result a, Result b) {
			return a.distance < b.distance ? -1 : a.distance == b.distance ? 0 : 1;
		}
	}

}