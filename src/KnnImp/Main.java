package KnnImp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class Main{

	private static double area, peremeter, compactness, lengthK, width, asymmetry, lengthKG;
    private static double [][] instances = new double[999][8];
    private static String [] check = new String[999];
    
    public static void main(String[] args) throws NumberFormatException, IOException {
        int k = 5;
        System.out.println("Welecome in KNN implementation");
        loadtrainData("C:\\Users\\urh\\Desktop\\seeds_dataset.txt");
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.print("Enter your K : ");
        k = scanner.nextInt();
        System.out.print("Wheat area : ");
        area = scanner.nextDouble();
        System.out.print("Wheat peremeter : ");
        peremeter = scanner.nextDouble();
        System.out.print("Wheat compactness : ");
        compactness = scanner.nextDouble();
        System.out.print("Wheat lengthK : ");
        lengthK = scanner.nextDouble();
        System.out.print("Wheat width : ");
        width = scanner.nextDouble();
        System.out.print("Wheat asymmetry : ");
        asymmetry = scanner.nextDouble();
        System.out.print("Wheat lengthKG : ");
        lengthKG = scanner.nextDouble();
        
    	KNN knn = new KNN(instances, check, k, area, peremeter, compactness, lengthK, width, asymmetry, lengthKG);
        knn.main();
        scanner.close();
    }
    
    public static void loadtrainData(String filename) throws NumberFormatException, IOException {
		File file = new File(filename);
		try {
			BufferedReader readFile = new BufferedReader(new FileReader(file));
			String line; int t = 0;
			while ((line = readFile.readLine()) != null) {
				String[] split = line.split(",");
				double[] feature = new double[split.length - 1];
				for (int i = 0; i < split.length - 1; i++)
					instances[t][i] = Double.parseDouble(split[i]);
				check[t] = split[feature.length];
				t++;
			}
			readFile.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}